class AddAutogeneratedPasswordToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :autogenerated_password, :boolean, default: false
  end
end
